# Phuongthaotrucchi

Tôi là Cao Phương Thảo. Định hướng và mong muốn xây dựng Trúc Chỉ Hà Nội là thương hiệu hàng đầu về sản phẩm nội thất phòng thờ. Chúng tôi thực hiện sứ mệnh tôn vinh, phát triển nét văn hóa truyền thống thờ cúng tổ tiên của người Việt.

- Địa chỉ: Chung cư sông nhuệ, Kiến Hưng, Hà Đông, Hà Nội

- SĐT: 0916938885

- Website: https://trucchihanoi.com/ve-kts-cao-phuong-thao

https://twitter.com/thaotrucchi

https://www.pinterest.com/phuongthaotrucchi/

https://www.flickr.com/people/198981282@N02/

https://www.twitch.tv/phuongthaotrucchi
